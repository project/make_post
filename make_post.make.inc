<?php
/**
 * @file
 *   Legacy support for POST methods downloads for drush make.
 */

/**
 * Download a file via the http POST method, using curl.
 */
function make_download_post($name, $download, $download_location) {

  // Prepare the temp location
  $tmp_path = make_tmp();
  $download_path = $tmp_path . '/__download__';
  $header_file = $tmp_path . '/__header__';
  drush_mkdir($download_path);
  touch($header_file);

  // Start downloading
  if (drush_shell_cd_and_exec($download_path, 'curl -d %s -LOD %s %s', $download['data'], $header_file, $download['url'])) {
    $download_mechanism = 'curl';
    $success = TRUE;
  }

  drush_shell_exec("ls %s", $download_path);
  $files_new = drush_shell_exec_output();
  $filename_tmp = $tmp_path . '/__download__/' . array_shift($files_new);

  // Determine the proper filename, or at least, the extension.
  $header = explode("\n", trim(str_replace("\r", '', file_get_contents($header_file))));
  $headers = array();
  $first = TRUE;
  foreach ($header as $h) {
    if (!$first) {
      list($key, $value) = explode(': ', $h, 2);
      $headers[$key] = $value;
    }
    else {
      $matches = array();
      preg_match('/HTTP\/1\.1 (\d\d\d)/', $h, $matches);
      if (isset($matches[1]) && ($matches[1]{0} == 4 || $matches[1]{0} == 5)) {
        $success = FALSE;
      }
      $first = FALSE;
    }
  }

  if (!$success) {
    return;
  }

  // Much more useful in reverse order.
  $headers = array_reverse($headers);
  $file = '';
  $headers = array_change_key_case($headers, CASE_LOWER);
  if (isset($headers['location'])) {
      $file = basename($header['location']);
  }

  if (isset($headers['content-disposition'])) {
    $parts = explode(';', $headers['content-disposition']);
    foreach ($parts as $part) {
      $inner_parts = explode('=', $part, 2);
      if (trim($inner_parts[0]) == 'filename') {
        $file = basename(trim($inner_parts[1], "\"'"));
        break;
      }
    }
  }
  if (!$file) {
    $file = basename($url);
  }

  if ($success) {
    drush_log(dt('%project downloaded from %url.', array('%project' => $name, '%url' => $download['url'])), 'ok');
    $filename = $tmp_path . '/' . $file;
    drush_shell_exec('mv %s %s', $filename_tmp, $filename);
    $download_location = make_download_file_unpack($filename, $download_location, (isset($download['filename']) ? $download['filename'] : ''));
    return $download_location;
  } 
  else {
    make_error('DOWNLOAD_ERROR', dt('Unable to download @project from @root @module.', 
      array('@project' => $name, '@root' => $download['root'], '@module' => $download['module'])));
    return FALSE;
  }
}
