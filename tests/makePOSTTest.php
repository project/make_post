<?php

/**
 * @file
 *   PHPUnit Tests for Drush Make POST support.
 *
 *   To run the tests, use phpunit --bootstrap=/path/to/drush/tests/drush_testcase.inc.
 *   Note that we are pointing to the drush_testcase.inc file under /tests subdir in drush.
 */

/**
 * Drush make-specific tests.
 */
class makePOSTDrushMakeTestCase extends Drush_CommandTestCase {
  /**
   * Run a given makefile test. Similar to tests in makeMakefileCase.
   *
   * @param $test
   *   The test makefile to run, as defined by $this->getMakefile();
   */
  private function runMakefileTest($test) {
    $default_options = array(
      // Add the --include option so the tests can find this code.
      'include' => dirname(__FILE__) . '/..',
      'test' => NULL,
      'md5' => 'print',
    );
    $makefile_path = dirname(__FILE__);
    $config = $this->getMakefile($test);
    $options = array_merge($config['options'], $default_options);
    $makefile = $makefile_path . '/' . $config['makefile'];
    $return = !empty($config['fail']) ? self::EXIT_ERROR : self::EXIT_SUCCESS;
    $this->drush('make', array($makefile), $options, NULL, NULL, $return);

    if (empty($config['fail'])) {
      // Check the log for the build hash.
      $output = $this->getOutputAsList();
      $this->assertEquals($output[0], $config['md5'], $config['name'] . ' - build md5 matches expected value: ' . $config['md5']);
    }
  }

  function testMakePOST() {
    $this->runMakefileTest('post');
  }

  function getMakefile($key) {
    static $tests = array(
      'post' => array(
        'name'     => 'Test POST retrieval of projects',
        'makefile' => 'post.make',
        'build'    => TRUE,
        'md5' => '4b61f10c13126b7ae438917cb00f1426',
        'options'  => array('no-core' => NULL),
      ),
    );
    return $tests[$key];
  }
}
